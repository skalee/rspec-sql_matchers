# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rspec/sql_matchers/version'

Gem::Specification.new do |spec|
  spec.name          = "rspec-sql_matchers"
  spec.version       = Rspec::SqlMatchers::VERSION
  spec.authors       = ["Sebastian Skałacki"]
  spec.email         = ["skalee@gmail.com"]
  spec.summary       = %q{Database-agnostic RSpec matchers for making assertions on SQL queries}
  spec.description   = spec.summary
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "cucumber", "~> 1.0"
  spec.add_development_dependency "aruba", "~> 0.6.0"
  spec.add_development_dependency "kpeg", "~> 0.8"

  spec.add_runtime_dependency "rspec-expectations", "~> 3.0"
end
