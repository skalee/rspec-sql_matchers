Dir['./spec/support/**/*'].each {|f| require f}

require "rspec/sql_matchers"

RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
  config.mock_with :rspec do |c|
    c.syntax = :should
  end
end
