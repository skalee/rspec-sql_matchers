require "spec_helper"

describe RSpec::SqlMatchers::Matchers::BeSqlEql do

  let(:diff_class){ RSpec::SqlMatchers::SqlParser::Diff }
  let(:ast_class){ RSpec::SqlMatchers::SqlParser::AST }

  before{ ast_class.stub(:new){ double } }

  describe "positive matching" do

    subject{ expect("actual").to be_sql_eql("expected") }

    it "passes when diff is empty" do
      diff_class.stub(:new).and_return(Set.new)
      subject
    end

    it "fails with proper message when one clause is different" do
      diff_class.stub(:new).and_return(Set.new [:some_sql_clause])
      expect{ subject }.to fail_with "expected to contain semantically equal SQL queries but some_sql_clause is different"
    end

    it "fails with proper message when many clauses are different" do
      diff_class.stub(:new).and_return(Set.new [:some_sql_clause, :other_clause, :one_more])
      expect{ subject }.to fail_with "expected to contain semantically equal SQL queries but some_sql_clause, other_clause and one_more are different"
    end

  end

  describe "negative matching" do

    subject{ expect("actual").not_to be_sql_eql("expected") }

    it "passes when diff is not empty" do
      diff_class.stub(:new).and_return(Set.new [:some_sql_clause])
      subject
    end

    it "fails with proper message when all clauses are equal" do
      diff_class.stub(:new).and_return(Set.new)
      expect{ subject }.to fail_with "expected to contain semantically different SQL queries but they are equal"
    end

  end

end
