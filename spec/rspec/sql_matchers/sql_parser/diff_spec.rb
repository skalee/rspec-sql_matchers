require "spec_helper"

describe RSpec::SqlMatchers::SqlParser::Diff do

  subject do
    lambda do |q1, q2|
      t1, t2 = [q1, q2].map{ |q| RSpec::SqlMatchers::SqlParser::AST.new q }
      described_class.new(t1, t2)
    end
  end

  specify "two identical strings containing SQL queries are equal" do
    subject.("SELECT column FROM table WHERE column = 5", "SELECT column FROM table WHERE column = 5").should be_empty
  end

  specify "when the only difference is case of SQL keywords they are equal" do
    subject.("SELECT column from table WHERE column = 5", "select column FROM table where column = 5").should be_empty
  end

  specify "when two strings have different spacing around SQL keywords they are equal" do
    subject.("SELECT*FROM table", "SELECT * FROM     table ").should be_empty
  end

  specify "when the difference is spacing in any clause but not inside string value then they are equal"



  it "detects semantically different SELECT clauses" do
    subject.("SELECT a, b FROM table", "SELECT b, a FROM table").tap do |s|
      s.should_not be_empty
      s.to_a.should == [:select]
    end
  end



  it "detects semantically different FROM clauses" do
    subject.("SELECT column FROM table_a", "SELECT column FROM table_b").tap do |s|
      s.should_not be_empty
      s.to_a.should == [:from]
    end
  end



  specify "when the difference is pair of paranthesis which could be skipped in WHERE clause they are equal" do
    subject.("SELECT * FROM table WHERE a = 1", "SELECT * FROM table WHERE (a = 1)").should be_empty
    subject.("SELECT * FROM table WHERE a = 1 OR b = 2", "SELECT * FROM table WHERE a = 1 OR (b = 2)").should be_empty
  end

  specify "when the difference is the order of commutative parts of WHERE clause they are equal" do
    subject.("SELECT * FROM table WHERE (a = 1 OR b = 2) AND c = 3", "SELECT * FROM table WHERE c = 3 AND (b = 2 OR a = 1)").should be_empty
  end

  it "detects that some query has more conditions in WHERE clause" do
    subject.("SELECT * FROM table WHERE column = 1", "SELECT * FROM table WHERE column = 1 OR column = 2").tap do |s|
      s.should_not be_empty
      s.to_a.should == [:where]
    end
  end

  it "detects that some query has different operator in WHERE clause" do
    subject.("SELECT * FROM table WHERE a = 1 OR b = 2", "SELECT * FROM table WHERE a = 1 AND b = 2").tap do |s|
      s.should_not be_empty
      s.to_a.should == [:where]
    end
  end

  it "detects different comparison in WHERE clauses" do
    subject.("SELECT * FROM table WHERE column = 1", "SELECT * FROM table WHERE column = 2").tap do |s|
      s.should_not be_empty
      s.to_a.should == [:where]
    end
  end

  it "detects difference in some logical operands in WHERE clauses", :focus do
    subject.("SELECT * FROM table WHERE a = 1 AND b = 2 OR c = 3", "SELECT * FROM table WHERE a = -1 AND b = 2 OR c = 3").tap do |s|
      s.should_not be_empty
      s.to_a.should == [:where]
    end
  end



  it "detects semantically different ORDER clauses" do
    subject.("SELECT column FROM table ORDER BY column ASC", "SELECT column FROM table ORDER BY column DESC").tap do |s|
      s.should_not be_empty
      s.to_a.should == [:order]
    end
  end

  it "detects semantically different LIMIT clauses" do
    subject.("SELECT column FROM table LIMIT 10", "SELECT column FROM table LIMIT 11").tap do |s|
      s.should_not be_empty
      s.to_a.should == [:limit]
    end
  end

  it "detects semantically different OFFSET clauses" do
    subject.("SELECT column FROM table OFFSET 10", "SELECT column FROM table OFFSET 11").tap do |s|
      s.should_not be_empty
      s.to_a.should == [:offset]
    end
  end



  describe "#new" do

    subject{ described_class.new :query1, :query2 }
    before{ described_class.any_instance.stub(:compare) }

    it "initializes underlying set" do
      hash = subject.instance_variable_get "@hash"
      hash.should == {}
    end

    it "performs comparison" do
      described_class.any_instance.should_receive :compare
      subject
    end

    it "assigns variables" do
      subject.instance_variable_get("@from").should == :query1
      subject.instance_variable_get("@to").should == :query2
    end

  end

end
