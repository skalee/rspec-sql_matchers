require 'spec_helper'

describe RSpec::SqlMatchers::SqlParser::AST do

  let(:ast_class){ RSpec::SqlMatchers::SqlParser::AST }
  let(:parser_class){ RSpec::SqlMatchers::SqlParser::Parser }
  let(:raise_parse_error){ raise_exception(RSpec::SqlMatchers::SqlParser::ParseError) }

  describe :new do

    subject{ ast_class.method :new }
    let(:query_string){ |example| example.description }



    context "accepted arguments" do

      before{ parser_class.stub(:new).with(String){ parser_dbl } }
      let(:parser_dbl){ double :parse => true, :result => :parse_result }

      it "accepts strings" do
        arg = "some query"
        ast = subject.(arg)
        ast.source.should == arg
        ast.source.should_not equal arg # different object
        ast.query.should == :parse_result
      end

      it "accepts objects which respond to #to_sql" do
        arg = double
        arg.should_receive(:to_sql){ "some query" }
        ast = subject.(arg)
        ast.source.should == "some query"
        ast.query.should == :parse_result
      end

      it "fails for objects which are not strings nor respond to #to_sql" do
        expect{ subject.(double) }.to raise_error ArgumentError
      end

    end



    context "most trivial select-from constructs" do

      example "SELECT column FROM table" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should be_nil
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

      example "SELECT * FROM table" do
        ast = subject.call query_string
        ast.query.select.should == '*'
        ast.query.from.should == 'table'
        ast.query.where.should be_nil
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

      example "SELECT*FROM table" do
        ast = subject.call query_string
        ast.query.select.should == '*'
        ast.query.from.should == 'table'
        ast.query.where.should be_nil
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

      example "SELECT a, b FROM table" do
        ast = subject.call query_string
        ast.query.select.should == 'a, b'
        ast.query.from.should == 'table'
        ast.query.where.should be_nil
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

    end



    context "simple select-from-where constructs" do

      example "SELECT column FROM table WHERE 1=1" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should == "1=1"
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

      example "SELECT column FROM table WHERE column = 'single quoted'" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should == "column = 'single quoted'"
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

      example "SELECT column FROM table WHERE column = \"double quoted\"" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should == "column = \"double quoted\""
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

      example "SELECT column FROM table WHERE column IN (1, 2, 3)" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should == "column IN (1, 2, 3)"
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

      example "SELECT column FROM table WHERE (column = 2)" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should == "column = 2"
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

      example "SELECT column FROM table WHERE column LIKE \"double quoted\"" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should == "column LIKE \"double quoted\""
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

    end



    context "composite select-from-where constructs" do

      example "SELECT column FROM table WHERE one = 1 AND two = 2 OR three = 3" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil

        ast.query.where.should be_a RSpec::SqlMatchers::SqlParser::Parser::AST::Condition
        ast.query.where.operator.should == "or"
        ast.query.where.operands[0].should be_a RSpec::SqlMatchers::SqlParser::Parser::AST::Condition
        ast.query.where.operands[0].operator.should == "and"
        ast.query.where.operands[0].operands[0].should == "one = 1"
        ast.query.where.operands[0].operands[1].should == "two = 2"
        ast.query.where.operands[1].should == "three = 3"
      end

      example "SELECT column FROM table WHERE (one = 1) AND ((two = 2) OR (three = 3))" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil

        ast.query.where.should be_a RSpec::SqlMatchers::SqlParser::Parser::AST::Condition
        ast.query.where.operator.should == "and"
        ast.query.where.operands[0].should == "one = 1"
        ast.query.where.operands[1].should be_a RSpec::SqlMatchers::SqlParser::Parser::AST::Condition
        ast.query.where.operands[1].operator.should == "or"
        ast.query.where.operands[1].operands[0].should == "two = 2"
        ast.query.where.operands[1].operands[1].should == "three = 3"
      end

      example "SELECT column FROM table WHERE (one = 1) AND (two = 2) AND (three = 3)" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil

        ast.query.where.should be_a RSpec::SqlMatchers::SqlParser::Parser::AST::Condition
        ast.query.where.operator.should == "and"
        ast.query.where.operands[0].should == "one = 1"
        ast.query.where.operands[1].should == "two = 2"
        ast.query.where.operands[2].should == "three = 3"
      end

    end



    context "constructs with order clauses" do

      example "SELECT column FROM table ORDER column ASC" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should be_nil
        ast.query.order.should == "column ASC"
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

      example "SELECT column FROM table ORDER column DESC" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should be_nil
        ast.query.order.should == "column DESC"
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

      example "SELECT column FROM table ORDER column1 DESC, column2 ASC" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should be_nil
        ast.query.order.should == "column1 DESC, column2 ASC"
        ast.query.limit.should be_nil
        ast.query.offset.should be_nil
      end

    end



    context "constructs with limit or offset clauses" do

      example "SELECT column FROM table LIMIT 10" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should be_nil
        ast.query.order.should be_nil
        ast.query.limit.should == "10"
        ast.query.offset.should be_nil
      end

      example "SELECT column FROM table OFFSET 20" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should be_nil
        ast.query.order.should be_nil
        ast.query.limit.should be_nil
        ast.query.offset.should == "20"
      end

      example "SELECT column FROM table LIMIT 10 OFFSET 20" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should be_nil
        ast.query.order.should be_nil
        ast.query.limit.should == "10"
        ast.query.offset.should == "20"
      end

    end



    context "SQL is case-insensitive" do

      example "select column from table where 1=1" do
        ast = subject.call query_string
        ast.query.select.should == 'column'
        ast.query.from.should == 'table'
        ast.query.where.should == '1=1'
      end

    end



    context "quoted fragments" do

      context "can contain escaped characters" do

        example %q[SELECT column FROM table WHERE column='\''] do
          ast = subject.call query_string
          ast.query.select.should == 'column'
          ast.query.from.should == 'table'
          ast.query.where.should == %q[column='\'']
        end

        example %q[SELECT column, '\'' AS ugly_word FROM table] do
          ast = subject.call query_string
          ast.query.select.should == %q[column, '\'' AS ugly_word]
          ast.query.from.should == 'table'
          ast.query.where.should be_nil
        end

      end

      context "can resemble SQL syntax elements" do

        example %q[SELECT column FROM table WHERE column='ORDER'] do
          ast = subject.call query_string
          ast.query.select.should == 'column'
          ast.query.from.should == 'table'
          ast.query.where.should == %q[column='ORDER']
        end

        example %q[SELECT column, 'ORDER' AS ugly_word FROM table] do
          ast = subject.call query_string
          ast.query.select.should == %q[column, 'ORDER' AS ugly_word]
          ast.query.from.should == 'table'
          ast.query.where.should be_nil
        end

        example %q[SELECT column, '(' AS ugly_word FROM table] do
          ast = subject.call query_string
          ast.query.select.should == %q[column, '(' AS ugly_word]
          ast.query.from.should == 'table'
          ast.query.where.should be_nil
        end

        example %q[SELECT column FROM "from"] do
          ast = subject.call query_string
          ast.query.select.should == 'column'
          ast.query.from.should == %q["from"]
          ast.query.where.should be_nil
        end

      end

    end



    context "improper order of query parts" do

      example "FROM table SELECT column" do
        expect{ subject.call query_string }.to raise_parse_error
      end

      example "SELECT column WHERE 1=1 FROM table" do
        expect{ subject.call query_string }.to raise_parse_error
      end

      example "SELECT column WHERE 1=1 FROM table LIMIT 10 ORDER column ASC" do
        expect{ subject.call query_string }.to raise_parse_error
      end

      example "SELECT column FROM table ORDER column ASC LIMIT 10 WHERE 1=1" do
        expect{ subject.call query_string }.to raise_parse_error
      end

      example "SELECT column FROM table WHERE 1=1 ORDER column ASC OFFSET 0 LIMIT 10" do
        expect{ subject.call query_string }.to raise_parse_error
      end

      example "SELECT column FROM table WHERE 1=1 LIMIT 10 OFFSET 0 ORDER column ASC" do
        expect{ subject.call query_string }.to raise_parse_error
      end

    end

  end

end
