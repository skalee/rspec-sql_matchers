Feature: RSpec 3 integration

  SQL matchers do integrate gently with RSpec.  You can use `be_sql_eql` matcher
  in your specs and, in case of example failure, you'll see some meaningful diff
  telling you which parts of the statement were different.


  Scenario: Positive and negative matchers with describe + it syntax
    Given a file named "spec/example_spec.rb" with:
      """ruby
        require "rspec/sql_matchers"

        describe "SELECT * FROM table" do

          it "equals identical query" do
            expect(subject).to be_sql_eql("SELECT * FROM table")
          end

          it "differs from query referencing another table" do
            expect(subject).not_to be_sql_eql("SELECT * FROM table2")
          end

        end
      """
    When I run `rspec spec/example_spec.rb`
    Then the examples should all pass


  Scenario: Positive and negative matchers with RSpec one-liner syntax
    Given a file named "spec/example_spec.rb" with:
      """ruby
        require "rspec/sql_matchers"

        describe "SELECT * FROM table" do

          it { is_expected.to be_sql_eql("SELECT * FROM table") }
          it { is_expected.not_to be_sql_eql("SELECT * FROM table2") }

        end
      """
    When I run `rspec spec/example_spec.rb`
    Then the examples should all pass


  Scenario: Meaningful failure message
    Given a file named "spec/example_spec.rb" with:
      """ruby
        require "rspec/sql_matchers"

        describe "SELECT * FROM table" do

          it { is_expected.to be_sql_eql("SELECT x FROM table2 WHERE y = 2") }

        end
      """
    When I run `rspec spec/example_spec.rb`
    Then the example should fail
    And the output should contain "expected to contain semantically equal SQL queries but select, from and where are different"
