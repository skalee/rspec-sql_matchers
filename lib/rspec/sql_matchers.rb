require "rspec/core"
# KPeg needs module-as-namespaces to be defined before requiring parser

module RSpec
  module SqlMatchers
    module SqlParser ; end
    module SqlMatchers ; end
  end
end

require "rspec/sql_matchers/version"

require "rspec/sql_matchers/matchers"
require "rspec/sql_matchers/matchers/be_sql_eql"

require "rspec/sql_matchers/sql_parser/parse_error"
require "rspec/sql_matchers/sql_parser/ast"
require "rspec/sql_matchers/sql_parser/diff"
require "rspec/sql_matchers/sql_parser/parser.kpeg"
