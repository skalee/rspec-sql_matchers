module RSpec
  module SqlMatchers
    module Matchers
      class BeSqlEql

        def initialize expected_src
          @expected = RSpec::SqlMatchers::SqlParser::AST.new expected_src
        end

        def matches? actual_src
          actual = RSpec::SqlMatchers::SqlParser::AST.new actual_src
          @diff = RSpec::SqlMatchers::SqlParser::Diff.new actual, @expected
          @diff.empty?
        end
        alias == matches?

        def failure_message
          clauses = @diff.reverse_each.with_index.inject "" do |memo, (attribute, index)|
            case index
            when 0 then attribute
            when 1 then "#{attribute} and #{memo}"
            else "#{attribute}, #{memo}"
            end
          end
          is_or_are = @diff.size == 1 ? "is" : "are"
          "expected to contain semantically equal SQL queries but #{clauses} #{is_or_are} different"
        end

        def failure_message_when_negated
          "expected to contain semantically different SQL queries but they are equal"
        end
        alias negative_failure_message failure_message_when_negated

        def description
          %Q[be a SQL query semantically equal to "#{@expected.source}"]
        end

      end
    end
  end
end
