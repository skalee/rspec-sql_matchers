require 'kpeg/compiled_parser'

class RSpec::SqlMatchers::SqlParser::Parser < KPeg::CompiledParser
  # :stopdoc:

  module AST
    class Node; end
    class Condition < Node
      def initialize(operator, operands)
        @operator = operator
        @operands = operands
      end
      attr_reader :operator
      attr_reader :operands
    end
    class Query < Node
      def initialize(select, from, where, order, limit, offset)
        @select = select
        @from = from
        @where = where
        @order = order
        @limit = limit
        @offset = offset
      end
      attr_reader :select
      attr_reader :from
      attr_reader :where
      attr_reader :order
      attr_reader :limit
      attr_reader :offset
    end
  end
  def condition(operator, operands)
    AST::Condition.new(operator, operands)
  end
  def query(select, from, where, order, limit, offset)
    AST::Query.new(select, from, where, order, limit, offset)
  end

  # root = query
  def _root
    _tmp = apply(:_query)
    set_failed_rule :_root unless _tmp
    return _tmp
  end

  # query = select:s from:f where:w? order?:o limit?:l offset?:os !. {query(s, f, w, o, l, os)}
  def _query

    _save = self.pos
    while true # sequence
      _tmp = apply(:_select)
      s = @result
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = apply(:_from)
      f = @result
      unless _tmp
        self.pos = _save
        break
      end
      _save1 = self.pos
      _tmp = apply(:_where)
      w = @result
      unless _tmp
        _tmp = true
        self.pos = _save1
      end
      unless _tmp
        self.pos = _save
        break
      end
      _save2 = self.pos
      _tmp = apply(:_order)
      @result = nil unless _tmp
      unless _tmp
        _tmp = true
        self.pos = _save2
      end
      o = @result
      unless _tmp
        self.pos = _save
        break
      end
      _save3 = self.pos
      _tmp = apply(:_limit)
      @result = nil unless _tmp
      unless _tmp
        _tmp = true
        self.pos = _save3
      end
      l = @result
      unless _tmp
        self.pos = _save
        break
      end
      _save4 = self.pos
      _tmp = apply(:_offset)
      @result = nil unless _tmp
      unless _tmp
        _tmp = true
        self.pos = _save4
      end
      os = @result
      unless _tmp
        self.pos = _save
        break
      end
      _save5 = self.pos
      _tmp = get_byte
      _tmp = _tmp ? nil : true
      self.pos = _save5
      unless _tmp
        self.pos = _save
        break
      end
      @result = begin; query(s, f, w, o, l, os); end
      _tmp = true
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_query unless _tmp
    return _tmp
  end

  # select = select_k < unspecified >
  def _select

    _save = self.pos
    while true # sequence
      _tmp = apply(:_select_k)
      unless _tmp
        self.pos = _save
        break
      end
      _text_start = self.pos
      _tmp = apply(:_unspecified)
      if _tmp
        text = get_text(_text_start)
      end
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_select unless _tmp
    return _tmp
  end

  # from = from_k < unspecified >
  def _from

    _save = self.pos
    while true # sequence
      _tmp = apply(:_from_k)
      unless _tmp
        self.pos = _save
        break
      end
      _text_start = self.pos
      _tmp = apply(:_unspecified)
      if _tmp
        text = get_text(_text_start)
      end
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_from unless _tmp
    return _tmp
  end

  # where = where_k conditions
  def _where

    _save = self.pos
    while true # sequence
      _tmp = apply(:_where_k)
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = apply(:_conditions)
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_where unless _tmp
    return _tmp
  end

  # order = order_k < unspecified >
  def _order

    _save = self.pos
    while true # sequence
      _tmp = apply(:_order_k)
      unless _tmp
        self.pos = _save
        break
      end
      _text_start = self.pos
      _tmp = apply(:_unspecified)
      if _tmp
        text = get_text(_text_start)
      end
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_order unless _tmp
    return _tmp
  end

  # limit = limit_k < unspecified >
  def _limit

    _save = self.pos
    while true # sequence
      _tmp = apply(:_limit_k)
      unless _tmp
        self.pos = _save
        break
      end
      _text_start = self.pos
      _tmp = apply(:_unspecified)
      if _tmp
        text = get_text(_text_start)
      end
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_limit unless _tmp
    return _tmp
  end

  # offset = offset_k < unspecified >
  def _offset

    _save = self.pos
    while true # sequence
      _tmp = apply(:_offset_k)
      unless _tmp
        self.pos = _save
        break
      end
      _text_start = self.pos
      _tmp = apply(:_unspecified)
      if _tmp
        text = get_text(_text_start)
      end
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_offset unless _tmp
    return _tmp
  end

  # conditions = cond_or
  def _conditions
    _tmp = apply(:_cond_or)
    set_failed_rule :_conditions unless _tmp
    return _tmp
  end

  # cond_or = (cond_and:l (or_k cond_and)+:r {condition('or', [l, *r])} | cond_and)
  def _cond_or

    _save = self.pos
    while true # choice

      _save1 = self.pos
      while true # sequence
        _tmp = apply(:_cond_and)
        l = @result
        unless _tmp
          self.pos = _save1
          break
        end
        _save2 = self.pos
        _ary = []

        _save3 = self.pos
        while true # sequence
          _tmp = apply(:_or_k)
          unless _tmp
            self.pos = _save3
            break
          end
          _tmp = apply(:_cond_and)
          unless _tmp
            self.pos = _save3
          end
          break
        end # end sequence

        if _tmp
          _ary << @result
          while true

            _save4 = self.pos
            while true # sequence
              _tmp = apply(:_or_k)
              unless _tmp
                self.pos = _save4
                break
              end
              _tmp = apply(:_cond_and)
              unless _tmp
                self.pos = _save4
              end
              break
            end # end sequence

            _ary << @result if _tmp
            break unless _tmp
          end
          _tmp = true
          @result = _ary
        else
          self.pos = _save2
        end
        r = @result
        unless _tmp
          self.pos = _save1
          break
        end
        @result = begin; condition('or', [l, *r]); end
        _tmp = true
        unless _tmp
          self.pos = _save1
        end
        break
      end # end sequence

      break if _tmp
      self.pos = _save
      _tmp = apply(:_cond_and)
      break if _tmp
      self.pos = _save
      break
    end # end choice

    set_failed_rule :_cond_or unless _tmp
    return _tmp
  end

  # cond_and = (cond_item:l (and_k cond_item)+:r {condition('and', [l, *r])} | cond_item)
  def _cond_and

    _save = self.pos
    while true # choice

      _save1 = self.pos
      while true # sequence
        _tmp = apply(:_cond_item)
        l = @result
        unless _tmp
          self.pos = _save1
          break
        end
        _save2 = self.pos
        _ary = []

        _save3 = self.pos
        while true # sequence
          _tmp = apply(:_and_k)
          unless _tmp
            self.pos = _save3
            break
          end
          _tmp = apply(:_cond_item)
          unless _tmp
            self.pos = _save3
          end
          break
        end # end sequence

        if _tmp
          _ary << @result
          while true

            _save4 = self.pos
            while true # sequence
              _tmp = apply(:_and_k)
              unless _tmp
                self.pos = _save4
                break
              end
              _tmp = apply(:_cond_item)
              unless _tmp
                self.pos = _save4
              end
              break
            end # end sequence

            _ary << @result if _tmp
            break unless _tmp
          end
          _tmp = true
          @result = _ary
        else
          self.pos = _save2
        end
        r = @result
        unless _tmp
          self.pos = _save1
          break
        end
        @result = begin; condition('and', [l, *r]); end
        _tmp = true
        unless _tmp
          self.pos = _save1
        end
        break
      end # end sequence

      break if _tmp
      self.pos = _save
      _tmp = apply(:_cond_item)
      break if _tmp
      self.pos = _save
      break
    end # end choice

    set_failed_rule :_cond_and unless _tmp
    return _tmp
  end

  # cond_item = (l_paren conditions:c r_paren {c} | unspecified)
  def _cond_item

    _save = self.pos
    while true # choice

      _save1 = self.pos
      while true # sequence
        _tmp = apply(:_l_paren)
        unless _tmp
          self.pos = _save1
          break
        end
        _tmp = apply(:_conditions)
        c = @result
        unless _tmp
          self.pos = _save1
          break
        end
        _tmp = apply(:_r_paren)
        unless _tmp
          self.pos = _save1
          break
        end
        @result = begin; c; end
        _tmp = true
        unless _tmp
          self.pos = _save1
        end
        break
      end # end sequence

      break if _tmp
      self.pos = _save
      _tmp = apply(:_unspecified)
      break if _tmp
      self.pos = _save
      break
    end # end choice

    set_failed_rule :_cond_item unless _tmp
    return _tmp
  end

  # unspecified = < unspecified_sgmt* > { text.strip }
  def _unspecified

    _save = self.pos
    while true # sequence
      _text_start = self.pos
      while true
        _tmp = apply(:_unspecified_sgmt)
        break unless _tmp
      end
      _tmp = true
      if _tmp
        text = get_text(_text_start)
      end
      unless _tmp
        self.pos = _save
        break
      end
      @result = begin;  text.strip ; end
      _tmp = true
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_unspecified unless _tmp
    return _tmp
  end

  # unspecified_sgmt = (< s_quote (!s_quote .)* s_quote > | < d_quote (!d_quote .)* d_quote > | < l_paren unspecified r_paren > | !keyword !l_paren !r_paren .)
  def _unspecified_sgmt

    _save = self.pos
    while true # choice
      _text_start = self.pos

      _save1 = self.pos
      while true # sequence
        _tmp = apply(:_s_quote)
        unless _tmp
          self.pos = _save1
          break
        end
        while true

          _save3 = self.pos
          while true # sequence
            _save4 = self.pos
            _tmp = apply(:_s_quote)
            _tmp = _tmp ? nil : true
            self.pos = _save4
            unless _tmp
              self.pos = _save3
              break
            end
            _tmp = get_byte
            unless _tmp
              self.pos = _save3
            end
            break
          end # end sequence

          break unless _tmp
        end
        _tmp = true
        unless _tmp
          self.pos = _save1
          break
        end
        _tmp = apply(:_s_quote)
        unless _tmp
          self.pos = _save1
        end
        break
      end # end sequence

      if _tmp
        text = get_text(_text_start)
      end
      break if _tmp
      self.pos = _save
      _text_start = self.pos

      _save5 = self.pos
      while true # sequence
        _tmp = apply(:_d_quote)
        unless _tmp
          self.pos = _save5
          break
        end
        while true

          _save7 = self.pos
          while true # sequence
            _save8 = self.pos
            _tmp = apply(:_d_quote)
            _tmp = _tmp ? nil : true
            self.pos = _save8
            unless _tmp
              self.pos = _save7
              break
            end
            _tmp = get_byte
            unless _tmp
              self.pos = _save7
            end
            break
          end # end sequence

          break unless _tmp
        end
        _tmp = true
        unless _tmp
          self.pos = _save5
          break
        end
        _tmp = apply(:_d_quote)
        unless _tmp
          self.pos = _save5
        end
        break
      end # end sequence

      if _tmp
        text = get_text(_text_start)
      end
      break if _tmp
      self.pos = _save
      _text_start = self.pos

      _save9 = self.pos
      while true # sequence
        _tmp = apply(:_l_paren)
        unless _tmp
          self.pos = _save9
          break
        end
        _tmp = apply(:_unspecified)
        unless _tmp
          self.pos = _save9
          break
        end
        _tmp = apply(:_r_paren)
        unless _tmp
          self.pos = _save9
        end
        break
      end # end sequence

      if _tmp
        text = get_text(_text_start)
      end
      break if _tmp
      self.pos = _save

      _save10 = self.pos
      while true # sequence
        _save11 = self.pos
        _tmp = apply(:_keyword)
        _tmp = _tmp ? nil : true
        self.pos = _save11
        unless _tmp
          self.pos = _save10
          break
        end
        _save12 = self.pos
        _tmp = apply(:_l_paren)
        _tmp = _tmp ? nil : true
        self.pos = _save12
        unless _tmp
          self.pos = _save10
          break
        end
        _save13 = self.pos
        _tmp = apply(:_r_paren)
        _tmp = _tmp ? nil : true
        self.pos = _save13
        unless _tmp
          self.pos = _save10
          break
        end
        _tmp = get_byte
        unless _tmp
          self.pos = _save10
        end
        break
      end # end sequence

      break if _tmp
      self.pos = _save
      break
    end # end choice

    set_failed_rule :_unspecified_sgmt unless _tmp
    return _tmp
  end

  # l_paren = s? "(" s?
  def _l_paren

    _save = self.pos
    while true # sequence
      _save1 = self.pos
      _tmp = apply(:_s)
      unless _tmp
        _tmp = true
        self.pos = _save1
      end
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = match_string("(")
      unless _tmp
        self.pos = _save
        break
      end
      _save2 = self.pos
      _tmp = apply(:_s)
      unless _tmp
        _tmp = true
        self.pos = _save2
      end
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_l_paren unless _tmp
    return _tmp
  end

  # r_paren = s? ")" s?
  def _r_paren

    _save = self.pos
    while true # sequence
      _save1 = self.pos
      _tmp = apply(:_s)
      unless _tmp
        _tmp = true
        self.pos = _save1
      end
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = match_string(")")
      unless _tmp
        self.pos = _save
        break
      end
      _save2 = self.pos
      _tmp = apply(:_s)
      unless _tmp
        _tmp = true
        self.pos = _save2
      end
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_r_paren unless _tmp
    return _tmp
  end

  # s_quote = "'"
  def _s_quote
    _tmp = match_string("'")
    set_failed_rule :_s_quote unless _tmp
    return _tmp
  end

  # d_quote = /"/
  def _d_quote
    _tmp = scan(/\A(?-mix:")/)
    set_failed_rule :_d_quote unless _tmp
    return _tmp
  end

  # s = /\s/+
  def _s
    _save = self.pos
    _tmp = scan(/\A(?-mix:\s)/)
    if _tmp
      while true
        _tmp = scan(/\A(?-mix:\s)/)
        break unless _tmp
      end
      _tmp = true
    else
      self.pos = _save
    end
    set_failed_rule :_s unless _tmp
    return _tmp
  end

  # we = (&/\W/ | !.)
  def _we

    _save = self.pos
    while true # choice
      _save1 = self.pos
      _tmp = scan(/\A(?-mix:\W)/)
      self.pos = _save1
      break if _tmp
      self.pos = _save
      _save2 = self.pos
      _tmp = get_byte
      _tmp = _tmp ? nil : true
      self.pos = _save2
      break if _tmp
      self.pos = _save
      break
    end # end choice

    set_failed_rule :_we unless _tmp
    return _tmp
  end

  # keyword = (select_k | from_k | where_k | order_k | limit_k | offset_k | or_k | and_k)
  def _keyword

    _save = self.pos
    while true # choice
      _tmp = apply(:_select_k)
      break if _tmp
      self.pos = _save
      _tmp = apply(:_from_k)
      break if _tmp
      self.pos = _save
      _tmp = apply(:_where_k)
      break if _tmp
      self.pos = _save
      _tmp = apply(:_order_k)
      break if _tmp
      self.pos = _save
      _tmp = apply(:_limit_k)
      break if _tmp
      self.pos = _save
      _tmp = apply(:_offset_k)
      break if _tmp
      self.pos = _save
      _tmp = apply(:_or_k)
      break if _tmp
      self.pos = _save
      _tmp = apply(:_and_k)
      break if _tmp
      self.pos = _save
      break
    end # end choice

    set_failed_rule :_keyword unless _tmp
    return _tmp
  end

  # select_k = /SELECT/i we
  def _select_k

    _save = self.pos
    while true # sequence
      _tmp = scan(/\A(?i-mx:SELECT)/)
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = apply(:_we)
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_select_k unless _tmp
    return _tmp
  end

  # from_k = /FROM/i we
  def _from_k

    _save = self.pos
    while true # sequence
      _tmp = scan(/\A(?i-mx:FROM)/)
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = apply(:_we)
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_from_k unless _tmp
    return _tmp
  end

  # where_k = /WHERE/i we
  def _where_k

    _save = self.pos
    while true # sequence
      _tmp = scan(/\A(?i-mx:WHERE)/)
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = apply(:_we)
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_where_k unless _tmp
    return _tmp
  end

  # order_k = /ORDER/i we
  def _order_k

    _save = self.pos
    while true # sequence
      _tmp = scan(/\A(?i-mx:ORDER)/)
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = apply(:_we)
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_order_k unless _tmp
    return _tmp
  end

  # limit_k = /LIMIT/i we
  def _limit_k

    _save = self.pos
    while true # sequence
      _tmp = scan(/\A(?i-mx:LIMIT)/)
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = apply(:_we)
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_limit_k unless _tmp
    return _tmp
  end

  # offset_k = /OFFSET/i we
  def _offset_k

    _save = self.pos
    while true # sequence
      _tmp = scan(/\A(?i-mx:OFFSET)/)
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = apply(:_we)
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_offset_k unless _tmp
    return _tmp
  end

  # and_k = /AND/i we
  def _and_k

    _save = self.pos
    while true # sequence
      _tmp = scan(/\A(?i-mx:AND)/)
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = apply(:_we)
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_and_k unless _tmp
    return _tmp
  end

  # or_k = /OR/i we
  def _or_k

    _save = self.pos
    while true # sequence
      _tmp = scan(/\A(?i-mx:OR)/)
      unless _tmp
        self.pos = _save
        break
      end
      _tmp = apply(:_we)
      unless _tmp
        self.pos = _save
      end
      break
    end # end sequence

    set_failed_rule :_or_k unless _tmp
    return _tmp
  end

  Rules = {}
  Rules[:_root] = rule_info("root", "query")
  Rules[:_query] = rule_info("query", "select:s from:f where:w? order?:o limit?:l offset?:os !. {query(s, f, w, o, l, os)}")
  Rules[:_select] = rule_info("select", "select_k < unspecified >")
  Rules[:_from] = rule_info("from", "from_k < unspecified >")
  Rules[:_where] = rule_info("where", "where_k conditions")
  Rules[:_order] = rule_info("order", "order_k < unspecified >")
  Rules[:_limit] = rule_info("limit", "limit_k < unspecified >")
  Rules[:_offset] = rule_info("offset", "offset_k < unspecified >")
  Rules[:_conditions] = rule_info("conditions", "cond_or")
  Rules[:_cond_or] = rule_info("cond_or", "(cond_and:l (or_k cond_and)+:r {condition('or', [l, *r])} | cond_and)")
  Rules[:_cond_and] = rule_info("cond_and", "(cond_item:l (and_k cond_item)+:r {condition('and', [l, *r])} | cond_item)")
  Rules[:_cond_item] = rule_info("cond_item", "(l_paren conditions:c r_paren {c} | unspecified)")
  Rules[:_unspecified] = rule_info("unspecified", "< unspecified_sgmt* > { text.strip }")
  Rules[:_unspecified_sgmt] = rule_info("unspecified_sgmt", "(< s_quote (!s_quote .)* s_quote > | < d_quote (!d_quote .)* d_quote > | < l_paren unspecified r_paren > | !keyword !l_paren !r_paren .)")
  Rules[:_l_paren] = rule_info("l_paren", "s? \"(\" s?")
  Rules[:_r_paren] = rule_info("r_paren", "s? \")\" s?")
  Rules[:_s_quote] = rule_info("s_quote", "\"'\"")
  Rules[:_d_quote] = rule_info("d_quote", "/\"/")
  Rules[:_s] = rule_info("s", "/\\s/+")
  Rules[:_we] = rule_info("we", "(&/\\W/ | !.)")
  Rules[:_keyword] = rule_info("keyword", "(select_k | from_k | where_k | order_k | limit_k | offset_k | or_k | and_k)")
  Rules[:_select_k] = rule_info("select_k", "/SELECT/i we")
  Rules[:_from_k] = rule_info("from_k", "/FROM/i we")
  Rules[:_where_k] = rule_info("where_k", "/WHERE/i we")
  Rules[:_order_k] = rule_info("order_k", "/ORDER/i we")
  Rules[:_limit_k] = rule_info("limit_k", "/LIMIT/i we")
  Rules[:_offset_k] = rule_info("offset_k", "/OFFSET/i we")
  Rules[:_and_k] = rule_info("and_k", "/AND/i we")
  Rules[:_or_k] = rule_info("or_k", "/OR/i we")
  # :startdoc:
end
