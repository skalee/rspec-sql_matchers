module RSpec
  module SqlMatchers
    module SqlParser
      class Diff < Set

        attr_reader :from, :to

        def initialize from, to
          super()
          @from, @to = from, to
          compare
        end

        def compare
          [:select, :from, :order, :limit, :offset].each do |clause|
            compare_by_equality clause
          end

          [:where].each do |clause|
            compare_conditions clause
          end
        end

      private

        def compare_by_equality clause
          unless from.query.send(clause) == to.query.send(clause)
            self << clause
          end
        end

        def compare_conditions clause
          node1, node2 = from.query.send(clause), to.query.send(clause)
          unless compare_condition_node node1, node2
            self << clause
          end
        end

        def compare_condition_node node1, node2
          oper_class = RSpec::SqlMatchers::SqlParser::Parser::AST::Condition

          if oper_class === node1 && oper_class === node2
            (node1.operator == node2.operator) && compare_operands(node1, node2)
          else
            node1 == node2
          end
        end

        def compare_operands node1, node2
          [[node1, node2], [node2, node1]].all? do |from, to|
            from.operands.all? do |f|
              to.operands.detect{ |t| compare_condition_node f, t }
            end
          end
        end

      end
    end
  end
end
