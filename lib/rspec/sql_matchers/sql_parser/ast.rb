module RSpec
  module SqlMatchers
    module SqlParser
      class AST

        attr_reader :query, :source

        def initialize query_string_or_object
          @source = prepare_query_string query_string_or_object
          parser = Parser.new @source
          parser.parse or raise ParseError
          @query = parser.result
        end

      private

        def prepare_query_string src
          if src.respond_to? :to_sql
            src.to_sql.freeze
          elsif src.kind_of? String
            src.dup.freeze
          else
            raise ArgumentError
          end
        end

      end
    end
  end
end
