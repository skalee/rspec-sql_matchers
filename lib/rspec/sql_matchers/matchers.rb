module RSpec
  module SqlMatchers
    module Matchers

      def be_sql_eql sql
        RSpec::SqlMatchers::Matchers::BeSqlEql.new sql
      end

    end
  end
end

RSpec.configure do |config|
  config.include RSpec::SqlMatchers::Matchers
end
